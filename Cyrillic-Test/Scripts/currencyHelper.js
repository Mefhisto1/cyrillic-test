﻿$(document).ready(() => {
    $('#viewExchangeRate').on('click', (e) => {
        if ($('#currency').val().trim() === '') {
            $('#resultLabel').text('Must not be empty');
            return;
        }

        if (/^[a-zA-Z]+$/.test($('#currency').val()) === false) {
            $('#resultLabel').text('Must use only letters');
            return;
        }

        if ($('#currency').val().trim().length < 3) {
            $('#resultLabel').text('Must enter 3 characters');
            return;
        }

        $('#resultLabel').text('');

        $.ajax({
            url: "https://api.exchangeratesapi.io/latest?symbols=" + $('#currency').val().toUpperCase(),
            success: function (data) {
                $('#resultLabel').text('Rate of ' + $('#currency').val().toUpperCase() + ' on ' + data.date + ' is: ' + data.rates[Object.keys(data.rates)[0]]);

                $.ajax({
                    url: '',
                    type: 'POST',
                    dataType: 'json',
                    data: { currency: $('#currency').val().toUpperCase()}
                });

            },
            error: function (data) {
                $('#resultLabel').text(data.responseJSON.error);
            }
        });
    });
});