﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cyrillic_Test.Startup))]
namespace Cyrillic_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
