﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyrillic.Models
{
    public class History
    {
        public string UserEmail { get; set; }
        public string Currency { get; set; }
        public DateTime Date { get; set; }
        public decimal CurrencyValue { get; set; }
    }
}