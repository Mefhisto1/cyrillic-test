﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cyrillic.Startup))]
namespace Cyrillic
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
