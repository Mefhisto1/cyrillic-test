﻿$(document).ready(() => {
    $('#viewExchangeRate').on('click', (e) => {
        if ($('#currency').val().trim() === '') {
            $('#resultLabel').text('Must not be empty');
            return;
        }

        if (/^[a-zA-Z]+$/.test($('#currency').val()) === false) {
            $('#resultLabel').text('Must use only letters');
            return;
        }

        if ($('#currency').val().trim().length < 3) {
            $('#resultLabel').text('Must enter 3 characters');
            return;
        }

        $('#resultLabel').text('');

        $.ajax({
            url: "https://api.exchangeratesapi.io/latest?symbols=" + $('#currency').val().toUpperCase(),
            success: function (data) {
                $('#resultLabel').text('Rate of ' + $('#currency').val().toUpperCase() + ' on ' + data.date + ' is: ' + data.rates[Object.keys(data.rates)[0]]);

                $.ajax({
                    url: '/api/RequestHistory',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        currencyName: $('#currency').val().toUpperCase(),
                        currencyValue: data.rates[Object.keys(data.rates)[0]]
                    }
                });

            },
            error: function (data) {
                $('#resultLabel').text(data.responseJSON.error);
            }
        });
    });


    $('#history').on('click', () => {
        $.ajax({
            url: '/api/RequestHistory/GetHistory',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $("#historyList").empty();

                data.forEach((h) => {
                    $("#historyList").append('<li>' + h.Date + ' ' + h.Currency + ' ' + h.CurrencyValue + '</li>');
                });
            },
            error: function (data) {
                $('#resultLabel').text(data.responseJSON.error);
            }
        });
    });
});