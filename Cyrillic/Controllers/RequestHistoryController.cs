﻿using Cyrillic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cyrillic_Test.Controllers
{
    public class RequestHistoryController : ApiController
    {
        [HttpGet]
        public List<History> GetHistory()
        {
            var userEmail = System.Web.HttpContext.Current.User.Identity.Name;

            using (SearchHistoryEntities entities = new SearchHistoryEntities())
            {
                return entities.Histories.Where(h => h.UserEmail == userEmail).ToList();
            }
        }

        [HttpPost]
        public async void Post(Currency currency)
        {
            using (SearchHistoryEntities entities = new SearchHistoryEntities())
            {
                entities.Histories.Add(new History
                {
                    Currency = currency.CurrencyName,
                    CurrencyValue = currency.CurrencyValue,
                    UserEmail = System.Web.HttpContext.Current.User.Identity.Name,
                    Date = DateTime.Now
                });

                await entities.SaveChangesAsync();
            }
        }
    }

    public class Currency
    {
        public string CurrencyName { get; set; }
        public decimal CurrencyValue { get; set; }
    }
}